import * as POSTBooks from '../requests/POSTBooks.request'
import * as BookOne from '../payloads/add-book-1.json'
import * as BookTwo from '../payloads/add-book-2.json'

context('Permitir o cadastro de livros e validar o retorno', () => {
    it('Adicionar o primeiro livro e validar o retorno', () => {
        POSTBooks.addBook(1).should((response) => {
            expect(response.status).to.eq(200);
            expect(response.body).to.be.not.null;
            expect(response.body.id).to.eq(BookOne.id);
            expect(response.body.title).to.eq(BookOne.title);
            expect(response.body.description).to.eq(BookOne.description);
            expect(response.body.pageCount).to.eq(BookOne.pageCount);
            expect(response.body.excerpt).to.eq(BookOne.excerpt);
            expect(response.body.publishDate).to.eq(BookOne.publishDate);
        })        
    });

    it('Adicionar o segundo livro e validar o retorno', () => {
        POSTBooks.addBook(2).should((response) => {
            expect(response.status).to.eq(200);
            expect(response.body).to.be.not.null;
            expect(response.body.id).to.eq(BookTwo.id);
            expect(response.body.title).to.eq(BookTwo.title);
            expect(response.body.description).to.eq(BookTwo.description);
            expect(response.body.pageCount).to.eq(BookTwo.pageCount);
            expect(response.body.excerpt).to.eq(BookTwo.excerpt);
            expect(response.body.publishDate).to.eq(BookTwo.publishDate);
        })        
    });
});