import * as GETBook from '../requests/GETBook.request'


context('Validar retorno de dados do livro desejado', () => {
    it('Recuperar os dados do livro e validar o retorno', () => {
        GETBook.getBook(100).should((response) => {
            expect(response.status).to.eq(200);
            expect(response.body).to.be.not.null;
            expect(response.body.id).to.be.a("number");
            expect(response.body.title).to.be.a("string");
            expect(response.body.description).to.be.a("string");
            expect(response.body.pageCount).to.be.a("number");
            expect(response.body.excerpt).to.be.a("string");
            expect(response.body.publishDate).to.be.a("string");
        })        
    });
});