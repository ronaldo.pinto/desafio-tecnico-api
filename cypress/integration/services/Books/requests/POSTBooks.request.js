/// <reference types="cypress" />

function addBook(idBook) {
    return cy.request({
        method: 'POST',
        url: 'Books',
        failOnStatusCode: false,
        body: require('../payloads/add-book-'+idBook+'.json')
    })
}

export { addBook };