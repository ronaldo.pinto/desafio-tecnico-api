// verbo/metodo - endpoint . motivo (request) . extensão
/// <reference types="cypress" />

function getBook(idBook){
    // cy.rquest - client http
    return cy.request({
        method: 'GET',
        url: `Books/${idBook}`,
        failOnStatusCode: false
    })
}

export{ getBook };